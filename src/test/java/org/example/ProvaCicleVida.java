package org.example;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

@DisplayName("Pruebas de ciclo de vida")
public class ProvaCicleVida {
    public ProvaCicleVida(){
        System.out.println("Constructor");
    }
    @BeforeAll
    static void setup(){
        System.out.println("@BeforeAll => setup(): Se ejecuta al principio de las pruebas");
    }

    @AfterAll
    static void tear(){
        System.out.println("@AfterAll => tear(): Se ejecuta al fina lde las pruebas");
    }
    @BeforeEach
    void setupThis(){
        System.out.println("@BeforeEach => setupThis(): Se ejecuta antes de cada prueba");
    }
    @AfterEach
    void tearThis(){
        System.out.println("@AfterEach => tearThis(): Se ejecuta despues de cada prueba");
    }
    @DisplayName("Primer test")
    @RepeatedTest(3)
    void testOne(){
        boolean isServerUp = Math.random()>= 0.5;
        assumeTrue(isServerUp,"Ignoring test, server is down!");
        int resultadoEsperado = 3;
        assertEquals(resultadoEsperado,1+2);
        System.out.println("========TEST UNO==========");
    }

    @RepeatedTest(value = 2,name = "---> {currentRepetition}/{totalRepetitions}")
    @DisplayName("Segundo test")
    void testTwo(){
        System.out.println("=========TEST DOS=========");
        int resultadoEsperado = 3;
        assertEquals(resultadoEsperado,1+2);
    }
}

package org.example;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Executable;
    import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumingThat;

public class ProvaCicleVidaActivittats {
    /**
     * Metodo encargado de comprobar dos valores. Si los valores no son iguales, el test saldra como valido
     * No obstante si los valores son iguales el test fallara
     */
    @Test
    void testAssertNotEquals(){
        assertNotEquals(2,3);
    }

    /**
     * El test sera valido si la condicion introducida es verdadera, en caso de ser falsa, el test falla
     */
    @Test
    void testAssertTrue(){
        assertTrue(true);
    }
    /**
     * Si la condicion proporcionada en este metodo es falsa, el test sera valido, si no, el test falla
     */
    @Test
    void testAssertFalse(){
        assertFalse(false);
    }

    /**
     * Si no se le proporciona un dato null a este metodo, el test falla
     */
    @Test
    void testAssertNull(){
        assertNull(null);
    }
    /**
     * Si el valor proporcionado es nulo, el test sera invalido
     */
    @Test
    void testAssertNotNull(){
        assertNotNull("si");
    }

    /**
     * Si el objeto esperado no es el mismo que el objeto pasado como parametro, el metodo falla
     */
    @Test
    void testAssertSame(){
        assertSame(1,1);
    }
    /**
     * Si el objeto esperado es igual al objeto pasado como parametro, el metodo falla
     */
    @Test
    void testAssertNotSame(){
        assertNotSame(1,2);
    }
    /**
     *Este metodo nos permite comprobar si una excepcion en concreto ha sido lanzada
     */
    @Test
    void testAssertThrows() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            throw new IllegalArgumentException("mensaje");
        });
    }

    /**
     * assumeFalse recibe una suposicion como parametro, si esta es falsa el test funcionara, si no, el test dara error
     */
    @Test
    void testAssumeFalse(){
        assumeFalse(false);
    }

    /**
     * Si la suposicion es valida, el metodo sera correcto. No obstante, si la suposicion es invalida, fallara
     */
    @Test
    void testAssumingThat(){
        assumingThat(false, () -> fail("Test fallido"));
    }
}


package org.example;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class MyCalculatorTest {
    private MyCalculator myCalculator;
    @BeforeEach
    void setUpMyCalculator(){
        myCalculator = new MyCalculator();
        System.out.println("My calculator created");
    }
    @AfterEach
    void tearDownMyCalculator(){
        myCalculator = null;
        System.out.println("My calculator = null");
    }
    @Disabled
    @Test
    void addWhenNegativeThrowsException(){
        int paramA = 10;
        int paramB = -5;
        IllegalArgumentException e =assertThrows(IllegalArgumentException.class, () -> {
            myCalculator.add(paramA,paramB);
        });

    }
    @Disabled
    @Test
    void addWhenNegativeThrowsException2(){
        int paramA = -10;
        int paramB = 5;
        IllegalArgumentException e =assertThrows(IllegalArgumentException.class, () -> {
            myCalculator.add(paramA,paramB);
        });

    }
    @Disabled
    @Test
    void subWhenNegativeThrowsException(){
        int paramA = 10;
        int paramB = -5;
        IllegalArgumentException e =assertThrows(IllegalArgumentException.class, () -> {
            myCalculator.sub(paramA,paramB);
        });

    }
    @Disabled
    @Test
    void subWhenNegativeThrowsException2(){
        int paramA = -10;
        int paramB = 5;
        IllegalArgumentException e =assertThrows(IllegalArgumentException.class, () -> {
            myCalculator.sub(paramA,paramB);
        });

    }
    @Disabled
    @Test
    void multWhenNegativeThrowsException(){
        int paramA = 10;
        int paramB = -5;
        IllegalArgumentException e =assertThrows(IllegalArgumentException.class, () -> {
            myCalculator.mult(paramA,paramB);
        });

    }
    @Disabled
    @Test
    void multWhenNegativeThrowsException2(){
        int paramA = -10;
        int paramB = 5;
        IllegalArgumentException e =assertThrows(IllegalArgumentException.class, () -> {
            myCalculator.mult(paramA,paramB);
        });

    }
    @Disabled
    @Test
    void divWhenNegativeThrowsException(){
        int paramA = 10;
        int paramB = -5;
        IllegalArgumentException e =assertThrows(IllegalArgumentException.class, () -> {
            myCalculator.div(paramA,paramB);
        });

    }
    @Disabled
    @Test
    void divWhenNegativeThrowsException2(){
        int paramA = -10;
        int paramB = 5;
        IllegalArgumentException e =assertThrows(IllegalArgumentException.class, () -> {
            myCalculator.div(paramA,paramB);
        });

    }

    @Test
    void add() {
    int actual = myCalculator.add(1,3);
    int expected = 4;

    assertEquals(expected,actual);
    }

    @Test
    void sub() {
        int actual = myCalculator.sub(5,5);
        int expected = 0;
        assertEquals(expected,actual);
    }

    @Test
    void mult() {
        int actual = myCalculator.mult(5,5);
        int expected = 25;
        assertEquals(expected,actual);
    }

    @Test
    @DisplayName("div =, cuando el denominador es distinto a 0")
    void div() {
        int actual = myCalculator.div(15,3);
        int expected = 5;
        assertEquals(expected,actual);
    }
    @Test
    @DisplayName("div- cuando el denominador es igual a 0")
    void divByZero(){
        int numerador = 10;
        int denominador = 0;
        ArithmeticException e =assertThrows(ArithmeticException.class, () -> {
            myCalculator.div(numerador,denominador);
        });
        assertEquals("Divisió per zero",e.getMessage());
    }
}